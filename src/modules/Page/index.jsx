import React, {
  useState,
  useEffect,
  // useRef
} from 'react';
import './index.scss'

import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

import NavBar from './components/nav-bar';
// import Home from './components/home';
import Home from './components/Home';
import About from './components/About';
import Projects from './components/Projects';
import Contact from './components/contact';
import Footer from './components/footer';
// import MobNavBar from './components/mobile-navBar';
import MiddlePicture from './components/middlePicture';
import useDarkMode from './components/darkMode';
import lightLogo from '../../shared/assets/images/longLogo.png'
import darkLogo from '../../shared/assets/images/longLogoDark.png'
import Logo from '../../shared/assets/images/logo.png'

export default function Page() {
  const [theme, toggleTheme] = useDarkMode();
  // const [visible, setVisible] = useState(false);
  const [mousePositionX, setMousePositionX] = useState(null);
  const [mousePositionY, setMousePositionY] = useState(null);
  const [scrollPositionY, setScrollPositionY] = useState(null);
  // const [isVisible, setIsVisible] = useState(false)


  useEffect(() => {
    window.addEventListener("mousemove", updateMousePosition);

    return () => window.removeEventListener("mousemove", updateMousePosition);
  }, []);

  const updateMousePosition = ev => {
    setMousePositionX(ev.pageX);
    setMousePositionY(ev.pageY);
  };

  useEffect(() => {
    window.addEventListener("scroll", updateScrollPosition);

    return () => window.removeEventListener("scroll", updateScrollPosition);
  }, []);

  const updateScrollPosition = ev => {
    setScrollPositionY(window.scrollY);
  };

  const arrowLink = document.querySelector('.arrowUp')

  arrowLink && arrowLink.addEventListener('click', () => {
    // projects && window.scrollTo({
    window.scrollTo({
      left: 0,
      // top: about.offsetTop,
      top: 0,
      behavior: 'smooth'
    })
  })

  // const darkColor = '#0F1218'
  const darkColor = '#1a233d'
  const fillColor = theme === 'light' ? '000000' : 'ffffff';


  let logoUrl = null
  theme === 'dark' ? logoUrl = Logo : logoUrl = Logo

  return (
    <div
      className="page"
      style={{ color: theme === 'dark' ? '#fff' : '#000', background: theme === 'dark' ? darkColor : '#fff' }}
    >
      <div
        className="logo"
        style={{ backgroundImage: `url(${logoUrl})` }}
      />
      <Tooltip
        title={
          theme === 'dark'
            ? 'Switch to light mode'
            : 'Switch to dark mode'
        }
        placement="bottom-start"
      >
        <IconButton className="themeIcon" type="button" onClick={toggleTheme} style={{ display: scrollPositionY < 100 ? 'block' : 'none' }}>
          <Icon style={{ color: theme === 'dark' ? '#f5fb20' : '#000', fontSize: 32, }}>{theme === 'dark' ? 'wb_sunny' : 'nights_stay'}</Icon>
        </IconButton>
      </Tooltip>
      <NavBar
        theme={theme}
      />
      <Home
        theme={theme}
        scrollPositionY={scrollPositionY}
      />
      <div className="mobileLayout">
        <About
          theme={theme}
        />
        <Projects
          theme={theme}
        />
        <MiddlePicture />
        <Contact
          theme={theme}
        />
        {/* <MobNavBar
          theme={theme}
        /> */}
        <Footer />
      </div>
      <img
        src={`https://img.icons8.com/ios-filled/50/${fillColor}/advance.png`}
        className='arrowUp'
        alt='img'
        style={{ display: scrollPositionY > 1300 ? 'block' : 'none' }}
      />
      <div className='cursor' style={{ top: mousePositionY, left: mousePositionX }} />
    </div >
  )
};
