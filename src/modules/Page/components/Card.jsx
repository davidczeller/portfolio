import React from 'react'
// import Img from './Images/shoe.png'
import './Card.scss';

export default function AnimatedCard({ content, title, imgUrl, subTitle, button }) {

  //Movement animation to happen
  const card = document.querySelector('.animatedCard')
  const container = document.querySelector('.container')
  //Optional
  const titleAnimation = document.querySelector('h1')
  const subTitleAnimation = document.querySelector('h3')
  const buttonAnimation = document.querySelector('.button')
  // const sneaker = document.querySelector('.sneaker img')
  // const purchase = document.querySelector('.purchase button')
  // const description = document.querySelector('h3')
  // const sizes = document.querySelector('.sizes')
  // const sizesActive = document.querySelector('.active')

  // Moving animation Event
  container && container.addEventListener('mousemove', e => {
    let xAxis = (window.innerWidth / 2 - e.pageX) / 25;
    let yAxis = (window.innerHeight / 2 - e.pageY) / 25;

    card.style.transform = `rotateY(${xAxis}deg) rotateX(${yAxis}deg)`
  })

  //Animate in
  container && container.addEventListener('mouseenter', e => {
    card.style.transition = 'none'
    //Popout
    titleAnimation.style.transform = 'translateZ(150px)'
    subTitleAnimation.style.transform = 'translateZ(120px)'
    buttonAnimation.style.transform = 'translateZ(175px)'
    // sneaker.style.transform = 'translateZ(200px) rotateZ(-45deg)'
    // description.style.transform = 'translateZ(120px)'
    // sizes.style.transform = 'translateZ(100px)'
    // sizesActive.style.transform = 'translateZ(225px)'
    // purchase.style.transform = 'translateZ(75px)'
  })
  //Animate Out
  container && container.addEventListener('mouseleave', e => {
    card.style.transition = 'all 0.5s ease-in'
    card.style.transform = 'rotateY(0deg) rotateX(0deg)'
    //Popback
    // title.styletransform = 'translateZ(0px)'
    // sneaker.style.transform = 'translateZ(0px) rotateZ(0deg)'
    // description.style.transform = 'translateZ(0px)'
    // sizes.style.transform = 'translateZ(0px)'
    // sizesActive.style.transform = 'translateZ(0px)'
    // purchase.style.transform = 'translateZ(0px)'
    titleAnimation.style.transform = 'translateZ(0px)'
    subTitleAnimation.style.transform = 'translateZ(0px)'
    buttonAnimation.style.transform = 'translateZ(0px)'
  })

  buttonAnimation && buttonAnimation.addEventListener('click', () => {
    // projects && window.scrollTo({    
    window.scrollTo({
      left: 0,
      // top: projects.offsetTop,
      top: 2600,
      behavior: 'smooth'
    })
  })


  return (
    <div className="App">
      <div className="container">
        <div className="animatedCard" style={{ backgroundImage: `url(${imgUrl})` }}>
          {/* <div className="sneaker"> */}
          {/* <div className="circle"> */}
          {/* <img src={Img} alt='shoe' /> */}
          {/* <img src='https://images.unsplash.com/photo-1553877690-c351cc96375a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1234&q=80'/> */}
          {/* </div> */}
          {/* </div> */}
          {/* <div className="info">
            <h1>Air Jordan.</h1>
            <h3>FUTURE-READY</h3>
          </div>
          <div className="sizes">
            <button>39</button>
            <button>39</button>
            <button className='active'>39</button>
            <button>39</button>
          </div>
          <div className="purchase">
            <button>Purchase</button>
          </div> */}
          <div className='animatedContent'>
            {content}
            <h1>{title}</h1>
            <h3>{subTitle}</h3>
            <button className='button'>{button}</button>
          </div>
        </div>
      </div>
    </div>
  );
}
