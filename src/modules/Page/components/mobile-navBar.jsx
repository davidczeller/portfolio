import React from 'react'

import './mobile-navBar.scss';
import {Link} from 'react-scroll'

// import Typography from '@material-ui/core/Typography';


export default function MobNavBar(props) {
  return (
    <div className="mobileContainer" style={{background: '#000', color: 'white'}}>
      <div className="mobileLinks">
        <Link
          activeClass="active"
          to="root"
          spy={true}
          smooth={true}
          duration={900}
        >
          Home
        </Link>
        <Link
          activeClass="active"
          to="about"
          spy={true}
          smooth={true}
          duration={900}
        >
          About
        </Link>
        <Link
          activeClass="active"
          to="contact"
          spy={true}
          smooth={true}
          duration={900}
        >
          Contact
        </Link>
        <a
          href="http://instagram.com/davidczeller"
          target="blank"
        >
          Works
        </a>
      </div>
    </div>
  )
};
