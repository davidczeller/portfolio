import React from 'react'

import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';

import './footer.scss';

const Footer = () => (
  <div className="footer">
    <a href="https://www.linkedin.com/in/david-czeller-018514136/" className='socialAnchor' target="blank">
      <img
        src={`https://img.icons8.com/ios/34/00000000/linkedin.png`}
        alt='img'
      />
    </a>
    <a href="https://www.github.com/davidczeller" target="blank" className='socialAnchor'>
      <img
        src={`https://img.icons8.com/ios/34/00000000/github.png`}
        alt='img'
      />
    </a>
    <a href="https://www.instagram.com/davidczeller/" target="blank" className='socialAnchor'>
      <img
        src={`https://img.icons8.com/ios/34/00000000/instagram-new.png`}
        alt='img'
      />
    </a>
    <IconButton
      className="cv"
      component="a"
      href="https://drive.google.com/file/d/1MbtnMNutCDjiTzHjWHYqXooavxqmAVF-/view"
      target="_blank"
    // type="button" 
    // onClick={toggleTheme}
    >
      <Icon style={{ color: '#20b1a9', fontSize: 32, padding: '0 !important' }}>
        {/* account_circle */}
        account_box
          </Icon>
    </IconButton>
  </div>
);

export default Footer;
