import React, { useState } from 'react';
import './nav-bar.scss';


const MENU_ITEMS = [
  { label: 'Home', id: 'root' },
  { label: 'About', id: 'about' },
  { label: 'Projects', id: 'project' },
  { label: 'Contact', id: 'contact' },
];

export default function Navbar(props) {
  const {
    theme,
  } = props;

  const scrollToMyRef = top =>
    window.scrollTo({
      top,
      left: 0,
      behavior: 'smooth'
    });

  const navToAnchor = id => {
    const el = document.getElementById(id);
    scrollToMyRef(el.offsetTop);
  };

  const renderItems = () =>
    MENU_ITEMS.map(v => (
      <button
        key={v.id}
        style={{ color: theme === 'dark' ? '#fff' : '#000' }}
        onClick={() => {
          navToAnchor(v.id)
          setClassName('nav-links')
          setHamburger('bar')
        }}
      >
        {v.label}
      </button>
    ));

  const [className, setClassName] = useState('nav-links')
  const [hamburger, setHamburger] = useState('bar')

  const bar = document.querySelector('.bar');

  if (bar) {
    theme !== 'dark'
      ? bar.style.backgroundColor = '#000'
      : bar.style.backgroundColor = '#fff'
  }
  return (
    <>
      <nav>
        <div
          className='hamburger'
          onClick={className === 'nav-links'
            ? () => {
              setClassName('nav-links open')
              setHamburger('hamburgerOpen')
            }
            : () => {
              setClassName('nav-links')
              setHamburger('bar')
            }
          }
        >
          <div className={hamburger} />
        </div>
        <ul className={className}
        >
          <div className='inner'>
            {renderItems()[0]}
            {renderItems()[1]}
            {renderItems()[2]}
            {renderItems()[3]}
            <a
              style={{ color: theme === 'dark' ? '#fff' : '#000' }}
              href="https://www.instagram.com/davidczeller"
              target="_blank"
              rel="noopener noreferrer"
            >
              Works
            </a>
          </div>
        </ul>
      </nav>
    </>
  )
}
