import React from 'react'
import './about.scss';
import classNames from 'classnames';

import Paper from '@material-ui/core/Paper';
import { Grid, Typography } from '@material-ui/core';

import Fade from 'react-reveal/Fade';

export default function About(props) {
  const {
    theme,
  } = props;

  const darkColor = '#0F1218'

  return (
    <div style={{ background: theme === 'dark' ? darkColor : '#fff', paddingTop: '24px' }} id="about">
      <Fade right>
        <Grid container alignItems="center" style={{}} className={classNames('mobileDisplay', 'bottomSpacing', 'topSpacing')}>
          <Grid item xs>
            <Paper
              className="paperLeft"
              style={{ color: theme === 'dark' ? '#fff' : '#000', background: theme === 'dark' ? darkColor : '#fff' }}
            >
              <Typography className="title" color="inherit"
                style={{
                  marginBottom: '4vh', fontWeight: 600,
                  // color: theme === 'dark' ? '#20b1a9' : '#000'
                  color: '#20b1a9'
                }}
              >
                My name is David Czeller. <br />
              </Typography>
              &nbsp;
              <Typography className="typography" color="inherit" >
                I'm a 27 years old Canadian-Hungarian based in Vienna.
                In the past 2 years, I dedicated a considerable amount of time and effort to learn and improve my skills in the IT field.
                In April of 2019, I got my first job as an intern.
                It amazed me how little I know and how endless this learning phase is, with a lot of potentials.
                I've done several courses online, been reading documentations, articles about this field.
            </Typography>
            </Paper>
          </Grid>
          <Grid item xs>
            <div className={classNames('picture', 'profilePicture')}></div>
          </Grid>
        </Grid>
      </Fade>
      <Fade left>
        <Grid container alignItems="center" className={classNames('mobileDisplay', 'bottomSpacing')}>
          <Grid item xs>
            <div className={classNames('picture', 'gfPicture')}></div>
          </Grid>
          <Grid item xs>
            <Paper
              className="paperRight"
              style={{ color: theme === 'dark' ? '#fff' : '#000', background: theme === 'dark' ? darkColor : '#fff' }}
            >
              <Typography className="title"
                style={{
                  fontWeight: 600,
                  // color: theme === 'dark' ? '#20b1a9' : '#000'
                  color: '#20b1a9'
                }}>
                Beginnings <br />
              </Typography>
              &nbsp;
              <Typography className="typography" color="inherit">
                I've used to work in the field of hospitality. I always wanted some more and in the summer of 2018, I finally decided to enter a Bootcamp in Hungary.
                I learned the basics with JAVA, but I realized fast, that back-end development is not for me, so I changed to Javascript.
                The course at Green Fox Academy involved a one and a half month long phase in which I have been working on a real-life project using Scrum methodology.
                My team was responsible for developing a web application for an in-house project. My main duties included front-end with React.js and UI development tasks.
            </Typography>
            </Paper>
          </Grid>
        </Grid>
      </Fade>
      <Fade right>
        <Grid container alignItems="center" className="bottomSpacing">
          <Grid item xs>
            <Paper
              className="paperLeft"
              style={{ color: theme === 'dark' ? '#fff' : '#000', background: theme === 'dark' ? darkColor : '#fff' }}
            >
              <Typography className="title"
                style={{
                  fontWeight: 600,
                  //  color: theme === 'dark' ? '#20b1a9' : '#000' 
                  color: '#20b1a9'
                }}>
                After the bootcamp <br />
              </Typography>
              &nbsp;
              <Typography className="typography" color="inherit">
                Started to look for my first job. Before I succeded I was doing some site build jobs, with my senior developer friends.
                I've also got hired for a project to redesign a website. I had working with the website owner real closely.
                So I've experienced that how to work with a client and how it is when I have to work on a real life project.
            </Typography>
            </Paper>
          </Grid>
          <Grid item xs>
            <div className={classNames('picture', 'itPeople')}></div>
          </Grid>
        </Grid>
      </Fade>
      <Fade left>
        <Grid container alignItems="center" className={classNames('mobileDisplay', 'bottomSpacing')}>
          <Grid item xs={12} md={6}>
            <div className={classNames('picture', 'palyaraFel')}></div>
          </Grid>
          <Grid item xs>
            <Paper
              className="paperRight"
              style={{ color: theme === 'dark' ? '#fff' : '#000', background: theme === 'dark' ? darkColor : '#fff' }}
            >
              <Typography className="title"
                style={{
                  fontWeight: 600,
                  // color: theme === 'dark' ? '#20b1a9' : '#000'
                  color: '#20b1a9'
                }}>
                Since I got my first job <br />
              </Typography>
              &nbsp;
              <Typography className="typography" color="inherit">
                I got my first job at a small project company. The time when I started there was only 4 of us at the company.
                Jumped right in the middle of a project, which was an online sport booking system.
                &nbsp; 
                <a href="https://palyarafel.hu/" rel="noopener norefferer" target="_blank" className="title" style={{ color: '#ec0298', textDecoration: 'none', cursor: 'pointer' }}>
                  Check it out!
                </a>
                <br />
                On the second project, which is an ERP system for a big Hungarian company I've participated right from the begginning,
                which means I was there as soon as we started planning it and we worked on it for about 6 to 8 months.
                My last project (at this company) was a web application for olympic sailing team.
                After I have worked on this project for a couple of months I decided to move to Vienna, Austria.
            </Typography>
              &nbsp; <br />
              <Typography className="title" style={{
                // color: theme === 'dark' ? '#20b1a9' : '#000' 
                color: '#20b1a9'
              }}>
                To be continued...
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      </Fade>
    </div>
  )
};