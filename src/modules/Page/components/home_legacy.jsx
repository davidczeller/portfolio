import React, { useState } from 'react';
import './home.scss';

import { Link } from 'react-scroll'
import { useScrollPosition } from '@n8tb1t/use-scroll-position'

import Icon from '@material-ui/core/Icon'
import Fade from 'react-reveal/Fade';

import backgroundDark from '../../../shared/assets/images/background.png'
import backgroundLight from '../../../shared/assets/images/background-light.png'
// import Parallax from './Parallax/Parallax';¬


export default function Home(props) {

  const [isVisible, setIsVisible] = useState(false)

  useScrollPosition(({ prevPos, currPos }) => {
    currPos.y > -600 ? setIsVisible(false) : setIsVisible(true);
  })

  return (
    <Fade top>
      {/* <Parallax className="showParallax"> */}
      {theme !== 'dark' ? (
        <div className="welcome">
          <div className="image" />
          <p
            className="welcomeText"
            style={{
              color: theme === 'dark' ? '#fff' : '#000'
            }}
          >
            David Czeller
        </p>
        </div>
      ) : (
          <div className="welcomeDark">
            <div className="image" />
            <p
              className="welcomeText"
              style={{
                color: theme === 'dark' ? '#fff' : '#000'
              }}
            >
              David Czeller
        </p>
          </div>
        )}
      {/* <div className="circleContainer"
        style={{
          background: theme === 'dark' ? 'rgb(21, 32, 43)' : '#fff'
        }}
      >
        <div
          className="circle"
        >
        </div>
        <div className="image" />
        <p
          className="welcomeText"
          style={{
            color: theme === 'dark' ? '#fff' : '#000'
          }}
        >
          David Czeller
        </p>
      </div> */}
      <Link activeClass="active" className="test8" to="about" spy={true} smooth={true} duration={900}>
        <Icon
          className={isVisible ? 'iconUpHidden' : 'icon'}
          style={{
            color: theme === 'dark' ? '#20b1a9' : '#000'
          }}
        >
          keyboard_arrow_down
        </Icon>
      </Link>
      <Link activeClass="active" className="test8" to="root" spy={true} smooth={true} duration={900}>
        <Icon
          className={isVisible ? "iconUp" : 'iconUpHidden'}
          style={{
            // color: '#fff'
            color: theme === 'dark' ? '#000' : '#000'
          }}
        >
          keyboard_arrow_up
        </Icon>
      </Link>
      {/* </Parallax> */}
    </Fade>
  )
}
