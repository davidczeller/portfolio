import React, { PureComponent } from 'react';

import classNames from 'classnames'
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
// import Icon from '@material-ui/core/Icon';
import { TextField } from '@material-ui/core';
import MeImg from '../../../shared/assets/images/dave.png'

import { MailerService } from '../../../core';
import './contact.scss';



class Contact extends PureComponent {
  constructor() {
    super();
    this.mailer = MailerService;
    this.state = {
      firstName: '',
      lastName: '',
      phoneNumber: '',
      email: '',
      reason: '',
      message: '',
      isFetching: false,
      open: false,
    };
  }

  send = async (event) => {
    event.preventDefault();
    try {
      await this.mailer.sendMail('default/contact', this.state, this.setState({ isFetching: true }));
      this.setState({ isFetching: false })
      this.setState({
        firstName: '',
        lastName: '',
        phoneNumber: '',
        email: '',
        reason: '',
        message: '',
        isFetching: false,
        open: false,
      });
    } catch (err) {
      alert('Something went wrong!');
      console.log(err);
    }
  };

  render() {
    // const {
    //   theme,
    // } = this.props;

    // const darkColor = '#0F1218';

    return (
      <div id="contact"
        className='mainContainer'
      // style={{ background: theme === 'dark' ? darkColor : '#fff' }}
      >
        <h1 className={classNames('title', 'projectTitle')} >
          Contact.
          </h1>
        <div className="contactContainer">
          <form onSubmit={this.send} className='form'>
            <div className="contact">
              {/* <h1 style={{ fontFamily: `'Black Ops One', cursive`, color: 'rgb(32, 177, 169)', marginBottom: 56 }}>Contact ME</h1> */}
              <TextField
                id="outlined-textarea"
                label="First Name"
                variant="outlined"
                required
                value={this.state.firstName}
                onChange={e => this.setState({ firstName: e.target.value })}
                autoComplete="new-password"
              />
              <TextField
                id="outlined-textarea"
                label="Last Name"
                variant="outlined"
                required
                value={this.state.lastName}
                onChange={e => this.setState({ lastName: e.target.value })}
                autoComplete="new-password"
              />
              <TextField
                id="outlined-textarea"
                label="Contact Number"
                variant="outlined"
                required
                type="tel"
                value={this.state.phoneNumber}
                onChange={e => this.setState({ phoneNumber: e.target.value })}
                autoComplete="new-password"
              />
              <TextField
                id="outlined-textarea"
                label="E-mail"
                variant="outlined"
                required
                type="email"
                value={this.state.email}
                onChange={e => this.setState({ email: e.target.value })}
                autoComplete="email"
              />
              <TextField
                id="outlined-textarea"
                label="Reason"
                variant="outlined"
                required
                value={this.state.reason}
                onChange={e => this.setState({ reason: e.target.value })}
              />
              <TextField
                id="outlined-textarea"
                label="Message"
                variant="outlined"
                multiline
                value={this.state.message}
                onChange={e => this.setState({ message: e.target.value })}
              />
              <button
                type="submit"
                style={{ position: 'relative', fontSize: '1.5rem' }}
              >
                {this.state.isFetching ? <CircularProgress className="circularProgress" /> : 'Send'}
                {/* <Icon style={{ color: '#20b1a9', fontSize: '1.5rem', position: 'absolute', right: '16px', top: '17px' }}>send</Icon> */}
              </button>
            </div>
          </form>
          {/* <div className='quote'>
            <p>"The only way to do great work is to love what you do!"</p>
            <span>Steve Jobs</span>
          </div>
          <img
            src={MeImg}
            alt='img'
          /> */}
          <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
            key={`qw`}
            style={{ zIndex: '10000' }}
            // open={this.state.isFetching}
            // onClose={this.state.isFetching}
            ContentProps={{
              'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">I love snacks</span>}
          />
          {/* <div className="flip-box">
            <div className="flip-box-inner">
              <div className="flip-box-front" />
              <a
                href="https://drive.google.com/file/d/1MbtnMNutCDjiTzHjWHYqXooavxqmAVF-/view"
                target="_blank"
              >
                <div className="flip-box-back" />
              </a>
            </div>
          </div> */}
        </div>
      </div>
    );
  }
}

export default Contact;
