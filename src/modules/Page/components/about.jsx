import React, { useRef } from 'react'

import classNames from 'classnames'

import ImageLeft from '../../../shared/assets/images/me1.png'
import ImageRight from '../../../shared/assets/images/me2.png'
import './About.scss'

export default function About() {
  let about = useRef(null);

  return (
    <div className='section' ref={el => about = el}>
      <div className={classNames('about')} id='about'>
        <h1
          className={classNames('title', 'aboutTitle')}
        >
          About.
      </h1>
      <div className='innerContainer'>
        <div className='inner'>
          <p
            className='aboutP'
          >
            &nbsp; I have dedicated my last two years to continuous learning and improvement in the field of IT.
            I completed an IT Bootcamp (Green Fox Academy) that offered me a great opportunity to start to build
            from the basics, surrounded by a dynamic and enthusiastic team. I learned the basics with JAVA, but
            later
            on switched to Javascript (ES/6) as it interests me on a higher level. The course at Green Fox
            Academy
            involved a one and a half month long phase in which I have been working on a real-life project using
            Scrum methodology.
            My team was responsible for developing a web application for an in-house project.
            My main duties included front-end with React.js and UI development tasks.
          <br />
            &nbsp; I got my first job at a small project company. The time when I started there was only 4 of us at the company.
            Jumped right in the middle of a project, which was an online sport booking system.
        <br />
            <a
              href="https://palyarafel.hu/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Check it out!
        </a>
            &nbsp; On the second project, which is an ERP system for a big Hungarian company I've participated right from the begginning,
            which means I was there as soon as we started planning it and we worked on it for about 6 to 8 months.
            My last project (at this company) was a web application for olympic sailing team.
            After I have worked on this project for a couple of months I decided to move to Vienna, Austria.
      </p>
        </div>
        <div className='images'>
          <img
            className={classNames('meImg', 'img-left')}
            src={ImageLeft}
            alt='img'
          />
          <img
            className={classNames('meImg', 'img-right')}
            src={ImageRight}
            alt='img2'
          />
        </div>
        </div>
        {/* <img src={Me} alt='me' className='meImg' /> */}
        {/* </Parallax>  */}
      </div>
    </div>
  )
}

