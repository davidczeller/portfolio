import React, { useRef } from 'react'

import classNames from 'classnames'

import Me from '../../../shared/assets/images/DavidCzellerOld.jpg'
import './Projects.scss'


export default function Projects() {
  let projects = useRef(null);

  const handleClick = (url) => {
    window.open(url)
    // window.location.href = url
  }

  return (
    <section
      className='project' id='project'
      ref={el => projects = el}
    >
      <h1 className={classNames('title', 'projectTitle')} >
        Projects.
    </h1>
      <div className='container'>
        <div className='card' onClick={() => handleClick('http://weather-app-react-practice.herokuapp.com/')} >
          <div className='overlay' />
          <img
            src={'https://images.unsplash.com/photo-1531324442324-909f6c0394e4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1234&q=80'}
            className='projectImg'
            alt='img'
          />

          <div>
            <h1>
              Weather App
        </h1>
            <p>responsive, real time weather forecast app.</p>
          </div>
        </div>
        <div className='card' onClick={() => handleClick('https://github.com/davidczeller/calculator')}>
          <div className='overlay' />
          <img
            src={'https://images.unsplash.com/photo-1587145820266-a5951ee6f620?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1700&q=80'}
            className='projectImg'
            alt='img'
          />
          <div>
            <h1>
              Calculator
        </h1>
            <p>inspired by iPhone.</p>
          </div>
        </div>
        <div className='card' onClick={() => handleClick('https://github.com/davidczeller/whatsApp-clone')}>
          <div className='overlay' />
          <img
            src={'https://images.unsplash.com/photo-1553877690-c351cc96375a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1234&q=80'}
            className='projectImg'
            alt='img'
          />

          <div>
            <h1>
              Messaging App
        </h1>
            <p>with real time update.</p>
          </div>
        </div>
        <div className='card' onClick={() => handleClick('https://bitbucket.org/davidczeller/portfolio/src/master/')}>
          <div className='overlay' />
          <img
            src={Me}
            className='projectImg'
            alt='img'
          />
          <div>
            <h1>
              Portfolio
        </h1>
            <p>source code of this website.</p>
          </div>
        </div>
      </div>
      <div className='stack'>
        <img
          src="https://img.icons8.com/color/64/000000/javascript.png"
          alt='img'
        />
        <img
          src="https://img.icons8.com/ultraviolet/64/000000/react.png"
          alt='img'
        />
        <img
          src="https://img.icons8.com/color/64/000000/sass.png"
          alt='img'
        />
        <img
          src="https://img.icons8.com/color/64/000000/html-5.png"
          alt='img'
        />
        <img
          src="https://img.icons8.com/color/64/000000/css3.png"
          alt='img'
        />
      </div>
    </section>
  )
}
