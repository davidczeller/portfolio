const parallaxStyle = {
  parallax: {
    height: "calc(100vh - 80px)",
    // width: "100%",
    overflow: "hidden",
    position: "absolute",
    backgroundPosition: "center center",
    backgroundSize: "cover",
    margin: "0",
    padding: "0",
    border: "0",
    alignItems: "center",
    // height: "100vh",
    // /* top: 0; */
    width: "100%",
    zIndex: 30,
  },
  filter: {
    "&:before": {
      background: "rgba(0, 0, 0, 0.5)"
    },
    "&:after,&:before": {
      position: "absolute",
      zIndex: "1",
      width: "100%",
      height: "100%",
      display: "block",
      left: "0",
      top: "0",
      content: "''"
    }
  },
  small: {
    // height: "380px"
  }
};

export default parallaxStyle;
