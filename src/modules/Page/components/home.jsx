import React, {
  useEffect,
  // useState,
  useRef
} from 'react'

import classNames from 'classnames'
import { Parallax } from 'react-parallax'
// import { gsap } from 'gsap'
import {
  Power3,
  // Timeline,
  TweenMax,
  // Elastic,
  // Circ
} from 'gsap'
// import { ScrollTrigger } from "gsap/ScrollTrigger"
// import { TextField, useMediaQuery } from '@material-ui/core';

// import InstaImg from '../../../shared/assets/images/instagram.png'
// import GithubImg from '../../../shared/assets/images/github.png'
// import Arrow from '../../../shared/assets/images/arrow.png'
// import ParallaxImage from '../../../shared/assets/images/dave2.png'
import ParallaxImage from '../../../shared/assets/images/me3.png'
import AnimatedCard from './Card';

import { useMediaQuery } from "@material-ui/core";

import './Home.scss'


export default function Home(props) {
  const {
    theme,
    // scrollPositionY,
  } = props;

  let linkedIn = useRef(null);
  let github = useRef(null);
  let instagram = useRef(null);
  let arrow = useRef(null);
  let image = useRef(null);
  let title = useRef(null);
  let subTitle = useRef(null);
  let buttonRef = useRef(null);
  // let about = document.getElementById('about').scrollIntoView();

  const fillColor = theme === 'light' ? '000000' : 'ffffff';

  const isMobile = useMediaQuery('(max-width:960px)');

  const cursor = document.querySelector('.cursor')
  const socialIcons = document.querySelectorAll('.social-icons a')
  const button = document.querySelector('.project-btn')
  const arrowLink = document.querySelector('.arrow')

  if (button) {
    theme === 'dark'
      ? button.style.color = 'rgb(255, 255, 255)'
      : button.style.color = 'rgb(0, 0, 0)'
  }

  socialIcons.forEach(anchor => {
    anchor.addEventListener('mouseover', () => {
      cursor.classList.add('link-grow')
    })

    anchor.addEventListener('mouseleave', () => {
      cursor.classList.remove('link-grow')
    })
  })

  //TODO fix the links
  button && button.addEventListener('click', () => {
    // projects && window.scrollTo({    
    window.scrollTo({
      left: 0,
      // top: projects.offsetTop,
      top: 2600,
      behavior: 'smooth'
    })
  })

  arrowLink && arrowLink.addEventListener('click', () => {
    // projects && window.scrollTo({
    window.scrollTo({
      left: 0,
      // top: about.offsetTop,
      top: 1400,
      behavior: 'smooth'
    })
  })

  useEffect(() => {
    // TweenMax.fromTo(title, 3, { opacity: 0 }, { opacity: 1, ease: Power3.easeInOut }, 5)
    // TweenMax.fromTo(subTitle, 3.5, { opacity: 0 }, { opacity: 1, ease: Power3.easeInOut }, 6)
    // TweenMax.fromTo(buttonRef, 3.5, { opacity: 0 }, { opacity: 1, ease: Power3.easeInOut }, 6)

    // TweenMax.fromTo(buttonRef, 3, { opacity: 0 }, { opacity: 1, ease: Power3.easeInOut }, 6)

    TweenMax.fromTo(linkedIn, 3, { opacity: 0 }, { opacity: 1, ease: Power3.easeInOut }, 3.2)
    TweenMax.fromTo(github, 4, { opacity: 0 }, { opacity: 1, ease: Power3.easeInOut }, 4.2)
    TweenMax.fromTo(instagram, 5, { opacity: 0 }, { opacity: 1, ease: Power3.easeInOut }, 5.2)
    TweenMax.fromTo(arrow, 6, { opacity: 0 }, { opacity: 1, ease: Power3.easeInOut }, 2.2)
  }, [])





  return (
    <div className={classNames('landing')}>
      <div ref={el => image = el}>
        {!isMobile ? (
          <AnimatedCard
            content={
              <Parallax bgImage={ParallaxImage} strength={1000} className='animatedBackgroundImg' >
              </Parallax>
            }
            title={'David Czeller'}
            subTitle={'Front End Developer'}
            button={'See Projects'}
          />
        ) : (
            <Parallax bgImage={ParallaxImage} strength={1000} className='backgroundImg' >
              <h2 className='sub-title' ref={el => subTitle = el}>
                Front End Developer
              </h2>
              <button
                className='project-btn'
                id='button'
                ref={el => buttonRef = el}
              >
                See Projects
        </button>
            </Parallax>
          )}
      </div>
      {isMobile && (
        <h1 className='title' ref={el => title = el}>
          David Czeller
        </h1>
      )}
      <div className='social-icons'>
        <div className='social-icons-inner'>
          <a href="https://www.linkedin.com/in/david-czeller-018514136/" className='socialAnchor' target="blank"
            ref={el => linkedIn = el}>
            <img
              src={`https://img.icons8.com/ios/40/${fillColor}/linkedin.png`}
              alt='img'
            />
          </a>
          <a href="https://www.github.com/davidczeller" target="blank" className='socialAnchor'
            ref={el => github = el}>
            <img
              src={`https://img.icons8.com/ios/40/${fillColor}/github.png`}
              alt='img'
            />
            {/* <img src={GithubImg} style={{ height: '22px', width: '22px' }} /> */}
          </a>
          <a href="https://www.instagram.com/davidczeller/" target="blank" className='socialAnchor'
            ref={el => instagram = el}>
            <img
              src={`https://img.icons8.com/ios/40/${fillColor}/instagram-new.png`}
              alt='img'
            />
            {/* <img src={InstaImg} style={{ height: '22px', width: '22px' }} /> */}
          </a>
        </div>
        <img
          src={`https://img.icons8.com/ios-filled/50/${fillColor}/advance.png`}
          className='arrow'
          ref={el => arrow = el}
          alt='img'
        />
      </div>
    </div>
  )
}
