class MailerService {
  constructor() {
    this.baseUrl = 'https://o8ek3v6ob7.execute-api.eu-west-1.amazonaws.com';
  }
  async sendMail(url, message) {
    try {
      const response = await fetch(`${this.baseUrl}/${url}`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(message),
        crossDomain: true
      });
      return response;
    } catch (error) {
      throw new Error(error);
    }
  }
}

export default new MailerService();
