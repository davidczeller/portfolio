import React, { Component } from 'react';
import './App.scss';
import Page from './modules/Page/index';

class App extends Component {
  render() {
    return (
      <Page/>
    );
  }
}

export default App;
